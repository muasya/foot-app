import os
from flask  import Flask, render_template, flash, redirect,url_for,session, request, logging
from flask_mysqldb import MySQL
from wtforms import Form, StringField, PasswordField, TextAreaField, validators
from passlib.hash import sha256_crypt

app =  Flask(__name__)




#config MySQL 
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'machakos12345'
app.config['MYSQL_DB'] = 'myfootapp'
app.config['MYSQL_CURSORCLASS']='DictCursor' 

#initMySQL
mysql = MySQL(app)

#news
@app.route('/')
def index():
    return render_template('news.html')

#leagues and cups
@app.route('/epl')
def leagues():
    return render_template('epl.html')

#registration
class RegisterForm(Form):
    name = StringField('Name', [validators.Length(min=1, max=50)])
    username = StringField('Username', [validators.Length(min=4, max=25)])
    email = StringField('Email', [validators.Length(min=6, max=50)])
    password = PasswordField('Password',[
        validators.DataRequired(),
        validators.EqualTo('confirm', message='Password do not match')
    ])
    confirm = PasswordField('Confirm Password')
@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm(request.form)
    if request.method == 'POST' and form.validate():
        name = form.name.data
        email = form.email.data
        username = form.username.data
        password = sha256_crypt.encrypt(str(form.password.data))
 
        #create cursor
        cur = mysql.connection.cursor()

        #Execute query
        cur.execute("INSERT INTO users(name, email, username,  password) VALUES(%s, %s, %s, %s)",(name, email, username, password)) 

        #Commit to DB
        mysql.connection.commit()
 
        #close connection
        cur.close()

        flash('You are now registered and can login','success')

        return redirect(url_for('login'))
    return render_template('register.html', form=form)


@app.route('/login',methods=['GET','POST'])
def login():
    if request.method == 'POST':
        #GET FORM FIELDS
        username = request.form['username']
        password_candidate = request.form['password']

        #Create cursor
        cur = mysql.connection.cursor()

        #Get user by username
        result = cur.execute("SELECT * FROM users WHERE username = %s", [username])

        if result > 0:
            #Get stored hash
            data = cur.fetchone()
            password = data['password']

            #Compare Passwords
            if sha256_crypt.verify(password_candidate, password):
                #Passed
                session['logged_in'] = True
                session['username'] = username

                flash('You are now logged in', 'success')
                return redirect(url_for('index'))
            else:
                 error = 'Password not matched'
                 return render_template('login.html',error=error)
            #Close connection
            cur.close()

        else:
           error = 'Username not found'
           return render_template('login.html',error=error)
    return render_template('login.html')




if __name__ == '__main__':
    app.secret_key='secret123'

    app.run(debug=True)